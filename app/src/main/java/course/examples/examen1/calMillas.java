package course.examples.examen1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class calMillas extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cal_millas);

        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);

        double millas = Double.parseDouble(message);
        millas = millas/0.62137;

        TextView textView = new TextView(this);
        textView.setTextSize(40);
        textView.setText(millas+" km");

        setContentView(textView);
    }
}
