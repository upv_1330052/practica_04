package course.examples.examen1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class calGrados extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cal_grados);

        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);

        double grados = Double.parseDouble(message);
        grados = (grados-32.0)/1.800;

        TextView textView = new TextView(this);
        textView.setTextSize(40);
        textView.setText(grados+" °C");

        setContentView(textView);
    }
}
